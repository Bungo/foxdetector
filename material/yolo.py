import cv2
import numpy as np


def load_labels(path):
    with open('foxy.names') as f:
        labels = [i.strip() for i in f.readlines()]
    return labels


def detect_objects(net, image, threshold, labels):
    inpSize = (608, 608)
    outNames = net.getUnconnectedOutLayersNames()

    blob = cv2.dnn.blobFromImage(image, 1./255, inpSize)

    net.setInput(blob)
    outs = net.forward(outNames)

    layerNames = net.getLayerNames()
    lastLayerId = net.getLayerId(layerNames[-1])
    lastLayer = net.getLayer(lastLayerId)

    results = []
    if lastLayer.type == 'Region':
        # Network produces output blob with a shape NxC where N is a number of
        # detected objects and C is a number of classes + 4 where the first 4
        # numbers are [center_x, center_y, width, height]
        for out in outs:
            for detection in out:
                scores = detection[5:]
                classId = np.argmax(scores)
                confidence = scores[classId]
                if confidence > threshold:
                    center_x = detection[0]
                    center_y = detection[1]
                    width = detection[2]
                    height = detection[3]

                    xmin = center_x - (width / 2)
                    ymin = center_y - (height / 2)
                    xmax = center_x + (width / 2)
                    ymax = center_y + (height / 2)

                    result = {
                        'bounding_box': [xmin, ymin, xmax, ymax],
                        'label': labels[classId],
                        'score': confidence
                    }

                    results.append(result)
    else:
        print('Unknown output layer type: ' + lastLayer.type)
        exit()

    return results
