import cv2
import numpy as np


def drawPred(image, bounding_box, label, score):
    height = image.shape[0]
    width = image.shape[1]

    xmin = int(bounding_box[0] * width)
    ymin = int(bounding_box[1] * height)
    xmax = int(bounding_box[2] * width)
    ymax = int(bounding_box[3] * height)

    # Draw a bounding box.
    cv2.rectangle(image, (xmin, ymin), (xmax, ymax),
                  (255, 0, 255), thickness=3)

    label_text = '%.2f' % score
    label_text = '%s: %s' % (label, label_text)

    cv2.putText(image, label_text, (xmin, ymin), cv2.FONT_HERSHEY_SIMPLEX,
                fontScale=1, color=(0, 255, 255), thickness=2)


# https://github.com/matterport/Mask_RCNN/blob/master/mrcnn/visualize.py
def apply_mask(image, mask, color, alpha=0.5):
    """Apply the given mask to the image.
    """
    mask = cv2.resize(np.float32(
        mask), (image.shape[1], image.shape[0]), interpolation=cv2.INTER_AREA)

    for c in range(3):
        image[:, :, c] = np.where(mask == 1,
                                  image[:, :, c] *
                                  (1 - alpha) + alpha * color[c] * 255,
                                  image[:, :, c])
    return image
