import datetime
import cv2
import os
import tensorflow as tf
import uuid

import mrcnn.model as modellib

import actions
import tflite
import yolo
import maskrcnn

from util import apply_mask, drawPred

MASKRCNN = False

if not os.path.exists("./fox_pictures"):
    os.makedirs("./fox_pictures")
if not os.path.exists("./person_pictures"):
    os.makedirs("./person_pictures")

capture = cv2.VideoCapture(0)

interpreter = tf.compat.v2.lite.Interpreter('detect.tflite')
interpreter.allocate_tensors()
tflite_labels = tflite.load_labels('coco_labels.txt')

net = cv2.dnn.readNet(f'yolov3-foxy_20000.weights', 'yolov3-foxy.cfg')
yolo_labels = yolo.load_labels('foxy.names')

if MASKRCNN:
    config = maskrcnn.InferenceConfig()
    model = modellib.MaskRCNN(
        mode="inference", config=config, model_dir='get-the-fox-master/')
    model.load_weights(
        'get-the-fox-master/mask_rcnn_get-the-fox_0010.h5', by_name=True)
    maskrcnn_labels = maskrcnn.load_labels()

while True:
    _, frame = capture.read()

    if MASKRCNN:
        maskrcnn_results = maskrcnn.detect_objects(
            model, frame, 0.4, maskrcnn_labels)
        for result in maskrcnn_results:
            if result['label'] == 'Person':
                actions.person_actions()
            elif result['label'] == 'Fox':
                drawPred(frame, result['bounding_box'],
                         result['label'], result['score'])
                frame = apply_mask(frame, result['mask'], (0, 255, 255))
                uid = uuid.uuid1()
                actions.fox_alert_actions(uid)
                cv2.imwrite(f'fox_pictures/{uid}.jpg', frame)
    else:
        frame_resized = cv2.resize(
            frame, (300, 300), interpolation=cv2.INTER_AREA)

        tflite_results = tflite.detect_objects(
            interpreter, frame_resized, 0.4, tflite_labels)
        yolo_results = yolo.detect_objects(net, frame, 0.4, yolo_labels)

        for result in tflite_results:
            if result['label'] == 'person':
                actions.person_actions()
                uid = uuid.uuid1()
                drawPred(frame, result['bounding_box'],
                         result['label'], result['score'])
                cv2.imwrite(f'person_pictures/{uid}.jpg', frame)

        for result in yolo_results:
            if result['label'] == 'Fox':
                drawPred(frame, result['bounding_box'],
                         result['label'], result['score'])
                uid = uuid.uuid1()
                actions.fox_alert_actions(uid)
                cv2.imwrite(f'fox_pictures/{uid}.jpg', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

capture.release()
cv2.destroyAllWindows()
