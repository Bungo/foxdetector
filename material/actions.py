import datetime
from notify_run import Notify
import subprocess

notify = Notify()

# Those 2 variables define the timeout after a detection of a fox or a human
# timeout means, how long the sound should not play after a detection
FOX_VISITING_TIMEOUT_SEC = 60
HUMAN_VISITING_TIMEOUT_SEC = 120

# init visiting time
fox_visiting_time = datetime.datetime(2000, 1, 1, 0, 0)
human_visiting_time = datetime.datetime(2000, 1, 1, 0, 0)


def fox_alert_actions(uuid):
    print("FOX IS VISITING!")
    global fox_visiting_time
    time_passed_since_last_visit = datetime.datetime.utcnow() - fox_visiting_time

    # emergency notification:
    notify.send(f'FOX IS VISITING! ({uuid})')

    if time_passed_since_last_visit.total_seconds() > FOX_VISITING_TIMEOUT_SEC:
        # play sound:
        bashCommand1 = "omxplayer -o local siren1.wav"
        bashCommand2 = "omxplayer -o local siren2.wav"
        bashCommand3 = "omxplayer -o local siren3-short.wav"
        subprocess.Popen(bashCommand1.split(), stdout=subprocess.PIPE)
        subprocess.Popen(bashCommand2.split(), stdout=subprocess.PIPE)
        subprocess.Popen(bashCommand3.split(), stdout=subprocess.PIPE)

        fox_visiting_time = datetime.datetime.utcnow()


def person_actions():
    print("PERSON IS VISITING!")
    global human_visiting_time
    time_passed_since_last_visit = datetime.datetime.utcnow() - human_visiting_time

    if time_passed_since_last_visit.total_seconds() > HUMAN_VISITING_TIMEOUT_SEC:
        # play sound:
        bashCommand = "omxplayer -o local hello-human.mp3"
        subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
        human_visiting_time = datetime.datetime.utcnow()
