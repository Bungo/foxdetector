import cv2
from mrcnn.config import Config
import mrcnn.model as modellib


class InferenceConfig(Config):
    NAME = 'get-the-fox'
    NUM_CLASSES = 1 + 5  # background + 5 classes
    IMAGES_PER_GPU = 1
    IMAGE_MIN_DIM = 256
    IMAGE_MAX_DIM = 256
    RPN_ANCHOR_SCALES = (16, 32, 64, 128, 256)


def load_labels():
    return ['BG', 'Fox', 'Person', 'Dog', 'Cat', 'Chicken']


def detect_objects(model, image, threshold, labels):
    temp = model.detect([image])
    r = temp[0]

    results = []
    for i in range(len(r['scores'])):
        rois = r['rois'][i]
        mask = r['masks'][:, :, i]
        classId = r['class_ids'][i]
        score = r['scores'][i]
        if score > threshold:
            height,  width = image.shape[:2]
            result = {
                'bounding_box': [rois[1] / width, rois[0] / height, rois[3] / width, rois[2] / height],
                'mask': mask,
                'label': labels[classId],
                'score': score
            }
            results.append(result)
    return results
