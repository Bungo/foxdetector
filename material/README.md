# Installation

```bash
git clone git@gitlab.com:fab1an/save-the-chicken.git
rm Pipfile.lock
pipenv install --skip-lock
pipenv shell
git clone https://github.com/matterport/Mask_RCNN.git
cd Mask_RCNN
pip3 install -r requirements.txt
python3 setup.py install
```

# Run

## Local Demo
```bash
python local.py
````

## Raspi Demo
```bash
LD_PRELOAD=/usr/lib/arm-linux-gnueabihf/libatomic.so.1 python chicken_saver.py
```